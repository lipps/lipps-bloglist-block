<?php

/**
 * Plugin Name: Lipps HairStyleSpec Block
 * Description: Custom block for Lipps
 * Version: 1.0.2
 * Author: Neulab Inc.
 *
 * @package lipps-hairstyle-spec-block
 */

defined( 'ABSPATH' ) || exit;

function lipps_hairstyle_spec_render( $attributes ) {
	$post_id = array_key_exists( 'post_id', $attributes ) ? $attributes['post_id'] : '';

	if ( get_post_type($post_id) !== 'hairstyles' ) {
		return '<p>この投稿タイプでは表示できません</p>';
	}

	$custom_field = get_post_meta( $post_id );
	$operation_array = array( 'cut' => 'カット', 'perm' => 'パーマ', 'treatment' => 'トリートメント', 'color' => 'カラー', 'scalp' => 'スキャルプ' );
	$hardness_array = array( 'soft' => '柔らかい', 'slightly_soft' => 'やや柔らかい', 'medium' => '普通', 'slightly_hard' => 'やや固い', 'hard' => '固い' );
	$volume_array = array( 'sparse' => '少ない', 'slightly_sparse' => 'やや少ない', 'medium' => '普通', 'slightly_dense' => 'やや多い', 'dense' => '多い');
	$curly_array = array( 'no' => 'なし', 'little' => 'ややあり', 'yes' => 'あり');
	$face_type_array = array( 'square' => '四角', 'triangle' => '逆三角', 'circle' => '丸', 'egg' => '卵', 'long' => '面長', 'base' => 'ベース' );
	$hairstyle_spec = array('hardness' => $hardness_array, 'volume' => $volume_array, 'curly' => $curly_array);
	$price = $custom_field['price'][0];

	$html = array( 'operation' => '', 'price' => '', 'hardness' => '', 'volume' => '', 'curly' => '', 'face_type' => '' );
	$unserialized = unserialize($custom_field['operation'][0]);
	foreach ( $operation_array as $key => $value ) {
		$html['operation'] .= array_search( $key, $unserialized) !== false ? $value . '/' : '';
	}
	$html['operation'] = rtrim($html['operation'], '/') ;
	$html['price'] = $price;

	foreach ( $hairstyle_spec as $category => $spec ) {
		$unserialized = unserialize($custom_field[$category][0]);
 		$num_column = $category === 'curly' ? ' three-column' : ' five-column';

		foreach ( $spec as $key => $value ) {
			$selected = array_search( $key, $unserialized ) !== false ? ' selected' : '';
			$html[$category] .= '<div class="spec-column' . $num_column . $selected . '">' . $value . '</div>';
		}
	}

	$unserialized = unserialize($custom_field['face_type'][0]);
	foreach ( $face_type_array as $key => $value ) {
		$selected = array_search( $key, $unserialized ) !== false ? ' selected' : '';
		$html['face_type'] .= '<dev class="face-type-box">';
		$html['face_type'] .= '<span class="face-type-icon '. $key . $selected . '"></span>';
        $html['face_type'] .= '<p class="face-type-word">' . $value . '</p>';
		$html['face_type'] .= '</dev>';
	}

	return sprintf('
        <div class="wp-block-lipps-lipps-hairstyle-spec-block">
           <p class="hairstyle-spec-menu">メニュー:</p>
           <div class="hairstyle-spec-row">
             <p class="hairstyle-spec-operation">%1$s</p>
             <p class="hairstyle-spec-price">%2$s</p>
           </div>
           <p class="hairstyle-spec-menu">髪質:</p>
           <div class="hairstyle-spec-row">%3$s</div>
           <p class="hairstyle-spec-menu">髪量:</p>
           <div class="hairstyle-spec-row">%4$s</div>
           <p class="hairstyle-spec-menu">クセ:</p>
           <div class="hairstyle-spec-row">%5$s</div>
           <p class="hairstyle-spec-menu">顔型:</p>
           <div class="hairstyle-spec-row">%6$s</div>
        </div>', $html['operation'], $html['price'], $html['hardness'], $html['volume'], $html['curly'], $html['face_type']
	);
}

function lipps_hairstyle_spec_register_block() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}

	wp_register_script(
		'lipps-hairstyle-spec-block',
		plugins_url( 'build/index.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-components'),
		filemtime( plugin_dir_path( __FILE__ ) . 'build/index.js' )
	);

	wp_register_style(
		'lipps-hairstyle-spec-block-editor',
		plugins_url( 'style.css', __FILE__ ),
		array( 'wp-edit-blocks' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
	);

	register_block_type( 'lipps/lipps-hairstyle-spec-block', array(
		'attributes' => array(
			'post_id' => array (
				'type' => 'integer'
			),
		),
		'style'    => 'lipps-hairstyle-spec-block-editor',
		'editor_style'    => 'lipps-hairstyle-spec-block-editor',
		'editor_script'   => 'lipps-hairstyle-spec-block',
		'render_callback' => 'lipps_hairstyle_spec_render'
	) );
}
add_action( 'init', 'lipps_hairstyle_spec_register_block' );

