const { registerBlockType } = wp.blocks;
const { ServerSideRender } = wp.components;
const { withSelect } = wp.data

registerBlockType( 'lipps/lipps-hairstyle-spec-block', {
	title: 'LIPPS-髪型詳述',
	icon: 'list-view',
	category: 'layout',
	attributes : {
		post_id: {
			type: 'Integer'
		},
	},

	edit: withSelect( (select) => {
		return {
			value: select('core/editor').getCurrentPostId()
		};
	} ) ( ( props ) => {
		if ( ! props.value ) {
			return "Loading..."
		}
		if ( props.value ) {
			props.setAttributes( { post_id: props.value } );
		}

		return (
			<ServerSideRender
		        block='lipps/lipps-hairstyle-spec-block'
		        attributes={ props.attributes } />
	    );
	} ),
	save : () => {
		return null;
	}
} );
